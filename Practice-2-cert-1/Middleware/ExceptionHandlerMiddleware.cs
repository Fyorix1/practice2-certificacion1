﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Serilog;
using System.Net;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;
using UPB.BusinessLogic.Managers.Exceptions;

namespace UPB.Practice_2_cert_1.Middleware
{
    public static class ExceptionHandlerMiddlewareExtensions
    {
        public static IApplicationBuilder UseExceptionHandlerMiddleware(this IApplicationBuilder Iappbuilder)
        {
            return Iappbuilder.UseMiddleware<ExceptionHandlerMiddleware>();
        }
    }

        public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate Next;

        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            Next = next;
        }

        public async Task Invoke(HttpContext HttpContext)
        {
            try
            {
                await Next(HttpContext);
            }
            catch(PatientAlreadyExistsException exception)
            {
                Log.Warning(exception.Message);
            }
            catch (Exception exception)
            {
                await execException(exception, HttpContext);
            }
        }

        private Task execException(Exception exception, HttpContext httpContext)
        {
            Log.Error(exception.Message);
            return httpContext.Response.WriteAsJsonAsync(new { Code = (int)HttpStatusCode.InternalServerError, Error = exception.Message });
        }
    }
}
