﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UPB.BusinessLogic.Models;
using UPB.BusinessLogic.Managers;

namespace UPB.Practice_2_cert_1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatientHTTP : ControllerBase
    {
        private readonly PatientManager PatientManager;

        public PatientHTTP(PatientManager patientManager)
        {
            PatientManager = patientManager;
        }

        [HttpGet]
        public List<Patient> Get()
        {
            return PatientManager.GetPatients();
        }

        [HttpGet]
        [Route("{ci}")]
        public Patient Get(int ci)
        {
            return PatientManager.GetPatientsByCI(ci);
        }

        [HttpPost]
        public async void Post([FromBody] Patient pt)
        {
            await PatientManager.CreatePatient(pt);
        }

        [HttpPut("{ci}")]
        public async void Put(int ci, [FromBody] Patient pt)
        {
            await PatientManager.UpdatePatient(ci, pt);
        }

        [HttpDelete("{ci}")]
        public void Delete(int ci)
        {
            PatientManager.Delete(ci);
        }

    }
}
