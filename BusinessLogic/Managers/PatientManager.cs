﻿﻿using Microsoft.Extensions.Configuration;
using Serilog;
using System.Net.Http.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPB.BusinessLogic.Managers;
using UPB.BusinessLogic.Managers.Exceptions;
using UPB.BusinessLogic.Models;

namespace UPB.BusinessLogic.Managers
{
    public class PatientManager
    {
        public List<Patient> Patients;
        public IConfiguration Iconfig;
        private HttpClient HTTPClient;

        public PatientManager(IConfiguration iconfiguration)
        {
            Patients = new List<Patient>();
            Iconfig = iconfiguration;
            HTTPClient = new HttpClient();
            PatientsToList();
        }
        private async Task<string> CreateHTTP(string name, string lastname, int ci)
        {
            var info = new { Name = name,
                Lastname = lastname,
                Ci = ci };
            var post = await HTTPClient.PostAsJsonAsync("http://localhost:5184/Patient", info);
            return await post.Content.ReadAsStringAsync();
        }

        public async Task<Patient> CreatePatient(Patient patient)
        {
            if (PatientExists(patient))
            {
                throw new PatientAlreadyExistsException();
            }
            string code = await CreateHTTP(patient.Name, patient.LastName, patient.CI);
            Patient createdPatient = new Patient()
            {
                Name = patient.Name,
                LastName = patient.LastName,
                CI = patient.CI,
                BloodGroup = GetRandomBloodGroup(),
                Code = code
            };

            Patients.Add(createdPatient);
            ListToFile();

            return createdPatient;
        }

        public async Task<Patient> UpdatePatient(int ci, Patient UpdatedPatient)
        {
            Patient? patientToUpdate = Patients.Find(x => x.CI == ci);

            if (patientToUpdate == null)
            {
                throw new PatientNotFoundException("UpdatePatient");
            }

            patientToUpdate.Name = UpdatedPatient.Name;
            patientToUpdate.LastName = UpdatedPatient.LastName;
            string code = await CreateHTTP(UpdatedPatient.Name, UpdatedPatient.LastName, UpdatedPatient.CI);
            patientToUpdate.Code = code;
            ListToFile();

            return patientToUpdate;
        }

        public List<Patient> Delete(int ci)
        {
            Patient? patientToDelete = Patients.Find(x => x.CI == ci);

            if (patientToDelete == null)
            {
                throw new PatientNotFoundException("Delete");
            }

            Patients.Remove(patientToDelete);

            ListToFile();

            return Patients;
        }

        public List<Patient> GetPatients()
        {
            return Patients;
        }

        public Patient GetPatientsByCI(int ci)
        {
            Patient? patientByCI = Patients.Find(x => x.CI == ci);

            if (patientByCI == null)
            {
                throw new PatientNotFoundException("GetPatientsByCI");
            }

            return patientByCI;
        }

        private static string GetRandomBloodGroup()
        {
            Random rd = new Random();
            int blood_rd = rd.Next(0, 8);
            string[] bloods = { "O-", "O+", "AB-", "AB+", "B+", "B-", "A-", "A+" };
            return bloods[blood_rd];
        }

        private void PatientsToList()
        {
            string? patientsFile = Iconfig.GetSection("FilePaths").GetSection("PatientFile").Value;

            if (patientsFile == null)
            {
                throw new JsonValueNotFoundException(["FilePaths", "PatientFile"]);
            }

            StreamReader sr = new StreamReader(patientsFile);

            Log.Information("Extracting patients data from file to list");

            Patients.Clear();

            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                string[] patientData = line.Split(",");

                Patient patient_read = new Patient()
                {
                    Name = patientData[0],
                    LastName = patientData[1],
                    CI = int.Parse(patientData[2]),
                    BloodGroup = patientData[3],
                    Code = patientData[4]
                };
                Patients.Add(patient_read);
            }
            sr.Close();
        }

        private void ListToFile()
        {
            string? patientsFile = Iconfig.GetSection("FilePaths").GetSection("PatientFile").Value;

            if (patientsFile == null)
            {
                throw new JsonValueNotFoundException(["FilePaths", "PatientFile"]);
            }

            StreamWriter sw = new StreamWriter(patientsFile);

            Log.Information("Patients are being saved to file");

            foreach (var patient in Patients)
            {
                string patientLine = patient.Name + "," + patient.LastName +
                 "," + $"{patient.CI}" + "," + patient.BloodGroup + "," + patient.Code;
                sw.WriteLine(patientLine);
            }
            sw.Close();
        }

        private bool PatientExists(Patient patient)
        {
            Patient? foundPatient = Patients.Find(pt => pt.CI == patient.CI);

            if (foundPatient == null)
                return false;
            return true;
        }
    }
}
