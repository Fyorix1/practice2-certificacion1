﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPB.BusinessLogic.Managers.Exceptions
{
    public class PatientNotFoundException : Exception
    {
        
        public PatientNotFoundException() : base("Patient not found") { }
        public PatientNotFoundException(string method) : base("From method : " + method + "Patient not found") { }

        public string GetErrorType()
        {
            return "Patient not found";
        }
    }
}
