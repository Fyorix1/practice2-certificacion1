﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPB.BusinessLogic.Managers.Exceptions
{
    public class JsonValueNotFoundException : Exception
    {
        private const string BaseMessage = "The Json value/section doesn't exist or is uncorrect";
        public JsonValueNotFoundException() : base(BaseMessage) { }

        public JsonValueNotFoundException(string[] sections) : base(BaseMessage + ".\nOnly these words can be used: " + string.Join(",", sections)) { }

        public string GetError()
        {
            return BaseMessage;
        }
    }
}
