﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPB.BusinessLogic.Managers.Exceptions
{
    public class PatientAlreadyExistsException : Exception
    {

        public PatientAlreadyExistsException() : base("The CI you wanted to add already exists, that means wether you have made a mistake with the CI number, or you wanted to add an existing patient and you can just update him") { }
    }
}
