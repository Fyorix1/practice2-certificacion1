﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPB.BusinessLogic.Models
{
    public class Patient
    {
        public string Name {  get; set; }
        public string LastName {  get; set; }

        public int CI {  get; set; }

        public string BloodGroup {  get; set; }
        public string Code { get; set; }
        public Patient() { }

        public Patient(string name, string lastname, int ci, string bloodgroup, string code)
        {
            Name = name;
            LastName = lastname;
            CI = ci;
            BloodGroup = bloodgroup;
            Code = code;
        }
    }
}
